var express = require('express');
var router = express.Router();

const multer = require("multer");
const path = require("path");

var fs = require('fs-extra'); 

// 저장될 파일 설정
// 어차피 최신만 쓸거라서 바꿔치기함
let storage = multer.diskStorage({
    destination : function(req,file,callback){
        // var getStroageName = sessionStorage.getItem("storageName")
        var existRootDirectory = fs.existsSync("./upload/");
        if(!existRootDirectory){
            fs.mkdirSync("./upload/");
        }

        callback(null,"upload/")
    },
    filename: function(req,file,callback){
        let extension = path.extname(file.originalname);
        let basename = path.basename(file.originalname,extension);

        callback(null,basename+extension);
    }
})

// 업로드 경로
let upload = multer({
    storage:storage 
})


// show 하면 라우터 
router.get('/show',function(req,res,next){
    res.render("board");
});

// 파일 업로드 처리 (action에서 읽어온 경로)
router.post('/create',upload.single('imgFile'),function(req,res,next)
{
    var storageName = req.body.storageName;
    var dirctory = "./upload/"+storageName;
    var existDirectory = fs.existsSync("./upload/"+storageName);

    if(!existDirectory){
        fs.mkdirSync(dirctory);
    }

    var file = req.file;
    
    if(file!=null){
        let result = {
            originalName : file.originalname,
            size : file.size
        }


        upload.single(file);
    
        
        console.log(JSON.stringify(result));
        // console.log(res);
        // res.json(result)
        res.send('1');
    }else{
        console.log("파일 업로드 필요!");
        res.send('-1');
    }

});

// 파일 업로드 처리 (action에서 읽어온 경로)
router.post('/moveFile',upload.single('fileName'),function(req,res,next)
{
    console.log("moveFile===>"+JSON.stringify(req.body));
    var fileName = req.body.fileName;
    var changeFileName = "contents.zip";
    var storageName = req.body.storageName;

    var oldpath = "./upload/"+fileName;
    var dirctory = "./upload/"+storageName+"/"+changeFileName;

    fs.rename(oldpath, dirctory, function(err){
        if(err) {
            res.send('-1');
        }else{
            console.log("success!!");
            res.send('1');
        }
    });

});


module.exports = router;

