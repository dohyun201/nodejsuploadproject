var express = require('express');
var app = express();

var uploads = require("./routes/upload");

app.get('/', function (req, res) {
    res.sendfile('./app/index.html');  
  // res.sendfile('./view/board.html');
    console.log('Hello World!');
});

app.get('/main', function (req, res) {
  res.sendfile('./view/board.html');
  console.log('Hello World!');
});
 
app.get('/download', function (req, res) { 
  // 파라미터 들어는것 캐치
  console.log(req.query.id);
  console.log(req.query.fileName);

  console.log('다운로드 시작!');
  var filename = req.query.fileName;
  console.log("filename===>"+filename);
  console.log("__dirname"+__dirname);
  // 상대경로 필요! 
  filepath = "./upload/"+req.query.id+"/contents.zip";
  console.log("filepath===>"+filepath);
  res.download(filepath);
});

// upload에 대한 js 참조 추가 
app.use('/upload',uploads);

// app.use('/upload',express.static('uploads'));
var server = app.listen(8000, function () {
  console.log('load Success!');
});
 